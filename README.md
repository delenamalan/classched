# Class scheduler
## Introduction
Welcome to my playground for learning about timetabling algorithms!

I have always wondered how schools and universities managed to coordinate the
timetables of all of their students, lecturers and rooms. I assumed that this
problem would be very much solved as it is such a prevalent problem and every
school and university seems to solve it quite easily. Some Googling revealed
that the problem is still recognised as difficult and that there was even a few
international timetabling competitions held. [The 2007 international
timetabling
competition (ITC)](http://www.cs.qub.ac.uk/itc2007/index_files/competitiontracks.htm))

The [ITC](http://www.cs.qub.ac.uk/itc2007/curriculmcourse/course_curriculm_index.htm)
describes the curriculum based course timetabling problem as follows:

> The Curriculum-based timetabling problem consists of the weekly
scheduling of the lectures for several university courses within a given number
of rooms and time periods, where conflicts between courses are set according to
the curricula published by the University and not on the basis of enrolment
data.

## How to run
Nothing to run yet, sorry.

## Backstory
Since my early high school years I have wondered how our school managed to
create our class timetables. I knew there had to be many variables and many
constraints. Individual teachers schedules and preferences, learners' subjects
choices, certain groups of classes that had to be together at certain times,
and various random constraints such as that mathematics were to be preferably
taught early in the morning.

After I first learned how to code, I set out to try and generate such a
timetable myself. However, I didn't know about arrays yet and soon realised that
I wouldn't be able to solve the problem with a set number of named variables.

Later, during my university years, I attempted to solve the problem again using
depth first search. I attempted this as a web application developed in MeteorJS.
I kept running into MeteorJS challenges and soon realised that simple depth
first search is not enough to solve this problem.

Now, I am back again.

## Input Variables 
The problem consists of the following variables:
- `numCourses`, 

   For every course:
  - `courseId`,
  - `teacher`,
  - `numLectures`,
  - `minWorkingDays`: Minimum number of days that the lectures of the course
    should be spread over, and
  - `numStudents`.


- `numRooms`, 

   For every room:
  - `roomId`, and
  - `capacity`: number of students who can occupy the room within a single
    period.


- `numDays`: the number of *lecturing* days within a given week,
- `periodsPerDay`: lecture timeslots in a single day,
- `numCurricula`

  For every curriculum:
  - `curriculumId`: a curriculum is a group of courses such that any pair of courses in
the group have students in common,
  - `numCourses`: the number of courses in the curriculum,
  - `courseIds`: a list of the course IDs in the curriculum,


- `numConstraints`. 

   For every constraint:
  - `courseId`,
  - `day`, and
  - `dayPeriod`: the period in the `day` in which the course with id `courseId`
    cannot be scheduled in.


## Constraints

### Hard constraints

### Soft constraints


Days, Timeslots, and Periods. We are given a number of teaching days in the week
(typically 5 or 6). Each day is split in a fixed number of timeslots, which is
equal for all days. A period is a pair composed by a day and a timeslot. The
total number of scheduling periods is the product of the days times the day
timeslots. 
Courses and Teachers. Each course consists of a fixed number of lectures to be
scheduled in distinct periods, it is attended by given number of students, and
is taught by a teacher. For each course there is a minimum number of days that
the lectures of the course should be spread in, moreover there are some periods
in which the course cannot be scheduled.
Rooms. Each room has a capacity, expressed in terms of number of available
seats. All rooms are equally suitable for all courses (if large enough).
Curricula. A curriculum is a group of courses such that any pair of courses in
the group have students in common. Based on curricula, we have the conflicts
between courses and other soft constraints.

The solution of the problem is an assignment of a period (day and timeslot) and
a room to all lectures of each course.
Hard constraints

Lectures: All lectures of a course must be scheduled, and they must be assigned
to distinct periods.

RoomOccupancy: Two lectures cannot take place in the same room in the same
period.

Conflicts: Lectures of courses in the same curriculum or taught by the same
teacher must be all scheduled in different periods.

Availabilities: If the teacher of the course is not available to teach that
course at a given period, then no lectures of the course can be scheduled at
that period.

Soft constraints

RoomCapacity: For each lecture, the number of students that attend the course
must be less or equal than the number of seats of all the rooms that host its
lectures.

MinimumWorkingDays: The lectures of each course must be spread into a minimum
number of days.

CurriculumCompactness: Lectures belonging to a curriculum should be adjacent to
each other (i.e., in consecutive periods). For a given curriculum we account for
a violation every time there is one lecture not adjacent to any other lecture
within the same day.

RoomStability: All lectures of a course should be given in the same room.


Name: ToyExample
Courses: 4
Rooms: 2
Days: 5
Periods_per_day: 4
Curricula: 2 
Constraints: 8

COURSES:
SceCosC Ocra 3 3 30
ArcTec Indaco 3 2 42
TecCos Rosa 5 4 40
Geotec Scarlatti 5 4 18

ROOMS:
A 32 
B 50

CURRICULA:
Cur1 3 SceCosC ArcTec TecCos
Cur2 2 TecCos Geotec

UNAVAILABILITY_CONSTRAINTS:
TecCos 2 0 
TecCos 2 1 
TecCos 3 2 
TecCos 3 3 
ArcTec 4 0 
ArcTec 4 1 
ArcTec 4 2 
ArcTec 4 3

END.


